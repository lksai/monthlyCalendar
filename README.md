# monthlyCalendar

#### 介绍

纯JS实现的一款月日历，不依赖第三方库

示例没有写样式

#### 示例

<a href="https://lksai.gitee.io/monthlycalendar/index.html" target="_blank">点击查看</a>

![输入图片说明](https://images.gitee.com/uploads/images/2021/0902/135606_ca53d727_1952090.png "屏幕截图.png")

